/*
* Copyright (C) 2013-2018 Jonathan Kelly jaknemsys@gmail.com
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
*    You should have received a copy of the License along with this program.
*    If not, see <https://www.gnu.org/licenses/>.
*
*    AUTO-GENERATED ORM WRAPPER
*
*    SELECT * FROM Recipes;
*/
//////////////////////////////////////////////////////////////////////
#ifndef RECIPE_H
#define RECIPE_H 1

#include <ulib/orm/orm.h>

class Recipe
{
public:
  U_MEMORY_TEST
  U_MEMORY_ALLOCATOR
  U_MEMORY_DEALLOCATOR

	uint32_t recipeID;
	UString title;
	uint32_t prep_time;
	uint32_t cook_time;
	uint32_t servings;
	UString catergory;
	UString cuisine;
	UString meat;
	UString directions;
	double carbs;
	double fiber;
	double protein;
	double fat;
	double kcal;

Recipe(){}

Recipe(uint32_t _recipeID,UString _title,uint32_t _prep_time,uint32_t _cook_time,uint32_t _servings,UString _catergory,UString _cuisine,UString _meat,UString _directions,double _carbs,double _fiber,double _protein,double _fat,double _kcal):
recipeID(_recipeID),title(_title),prep_time(_prep_time),cook_time(_cook_time),servings(_servings),catergory(_catergory),cuisine(_cuisine),meat(_meat),directions(_directions),carbs(_carbs),fiber(_fiber),protein(_protein),fat(_fat),kcal(_kcal){}

void bindParam(UOrmStatement* stmt){
stmt->bindParam(U_ORM_TYPE_HANDLER(recipeID, uint32_t));
stmt->bindParam(U_ORM_TYPE_HANDLER(title, UString));
stmt->bindParam(U_ORM_TYPE_HANDLER(prep_time, uint32_t));
stmt->bindParam(U_ORM_TYPE_HANDLER(cook_time, uint32_t));
stmt->bindParam(U_ORM_TYPE_HANDLER(servings, uint32_t));
stmt->bindParam(U_ORM_TYPE_HANDLER(catergory, UString));
stmt->bindParam(U_ORM_TYPE_HANDLER(cuisine, UString));
stmt->bindParam(U_ORM_TYPE_HANDLER(meat, UString));
stmt->bindParam(U_ORM_TYPE_HANDLER(directions, UString));
stmt->bindParam(U_ORM_TYPE_HANDLER(carbs, double));
stmt->bindParam(U_ORM_TYPE_HANDLER(fiber, double));
stmt->bindParam(U_ORM_TYPE_HANDLER(protein, double));
stmt->bindParam(U_ORM_TYPE_HANDLER(fat, double));
stmt->bindParam(U_ORM_TYPE_HANDLER(kcal, double));
}

void bindResult(UOrmStatement* stmt){
stmt->bindResult(U_ORM_TYPE_HANDLER(recipeID, uint32_t));
stmt->bindResult(U_ORM_TYPE_HANDLER(title, UString));
stmt->bindResult(U_ORM_TYPE_HANDLER(prep_time, uint32_t));
stmt->bindResult(U_ORM_TYPE_HANDLER(cook_time, uint32_t));
stmt->bindResult(U_ORM_TYPE_HANDLER(servings, uint32_t));
stmt->bindResult(U_ORM_TYPE_HANDLER(catergory, UString));
stmt->bindResult(U_ORM_TYPE_HANDLER(cuisine, UString));
stmt->bindResult(U_ORM_TYPE_HANDLER(meat, UString));
stmt->bindResult(U_ORM_TYPE_HANDLER(directions, UString));
stmt->bindResult(U_ORM_TYPE_HANDLER(carbs, double));
stmt->bindResult(U_ORM_TYPE_HANDLER(fiber, double));
stmt->bindResult(U_ORM_TYPE_HANDLER(protein, double));
stmt->bindResult(U_ORM_TYPE_HANDLER(fat, double));
stmt->bindResult(U_ORM_TYPE_HANDLER(kcal, double));
}

void toJSON(UString& json){
U_TRACE(5, "Recipe::toJSON(%p)", &json)
json.toJSON(U_JSON_METHOD_HANDLER(recipeID, uint32_t));
json.toJSON(U_JSON_METHOD_HANDLER(title, UString));
json.toJSON(U_JSON_METHOD_HANDLER(prep_time, uint32_t));
json.toJSON(U_JSON_METHOD_HANDLER(cook_time, uint32_t));
json.toJSON(U_JSON_METHOD_HANDLER(servings, uint32_t));
json.toJSON(U_JSON_METHOD_HANDLER(catergory, UString));
json.toJSON(U_JSON_METHOD_HANDLER(cuisine, UString));
json.toJSON(U_JSON_METHOD_HANDLER(meat, UString));
json.toJSON(U_JSON_METHOD_HANDLER(directions, UString));
json.toJSON(U_JSON_METHOD_HANDLER(carbs, double));
json.toJSON(U_JSON_METHOD_HANDLER(fiber, double));
json.toJSON(U_JSON_METHOD_HANDLER(protein, double));
json.toJSON(U_JSON_METHOD_HANDLER(fat, double));
json.toJSON(U_JSON_METHOD_HANDLER(kcal, double));
}

void fromJSON(UValue& json){
U_TRACE(5, "Recipe::fromJSON(%p)", &json)
json.fromJSON(U_JSON_METHOD_HANDLER(recipeID, uint32_t));
json.fromJSON(U_JSON_METHOD_HANDLER(title, UString));
json.fromJSON(U_JSON_METHOD_HANDLER(prep_time, uint32_t));
json.fromJSON(U_JSON_METHOD_HANDLER(cook_time, uint32_t));
json.fromJSON(U_JSON_METHOD_HANDLER(servings, uint32_t));
json.fromJSON(U_JSON_METHOD_HANDLER(catergory, UString));
json.fromJSON(U_JSON_METHOD_HANDLER(cuisine, UString));
json.fromJSON(U_JSON_METHOD_HANDLER(meat, UString));
json.fromJSON(U_JSON_METHOD_HANDLER(directions, UString));
json.fromJSON(U_JSON_METHOD_HANDLER(carbs, double));
json.fromJSON(U_JSON_METHOD_HANDLER(fiber, double));
json.fromJSON(U_JSON_METHOD_HANDLER(protein, double));
json.fromJSON(U_JSON_METHOD_HANDLER(fat, double));
json.fromJSON(U_JSON_METHOD_HANDLER(kcal, double));
}

#ifdef DEBUG
const char* dump(bool breset) const
{ return U_NULLPTR; }
#endif 

private:
U_DISALLOW_ASSIGN(Recipe);
};
#endif
//////////////////////////////////////////////////////////////////////