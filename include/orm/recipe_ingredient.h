/*
* Copyright (C) 2013-2018 Jonathan Kelly jaknemsys@gmail.com
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
*    You should have received a copy of the License along with this program.
*    If not, see <https://www.gnu.org/licenses/>.
*
*    AUTO-GENERATED ORM WRAPPER
*
*    SELECT * FROM RecipeIngredients;
*/
//////////////////////////////////////////////////////////////////////
#ifndef RECIPEINGREDIENT_H
#define RECIPEINGREDIENT_H 1

#include <ulib/orm/orm.h>

class RecipeIngredient
{
public:
  U_MEMORY_TEST
  U_MEMORY_ALLOCATOR
  U_MEMORY_DEALLOCATOR

	uint32_t recipeID;
	uint32_t ingredientID;
	double quantity;
	uint32_t unitID;

RecipeIngredient(){}

RecipeIngredient(uint32_t _recipeID,uint32_t _ingredientID,double _quantity,uint32_t _unitID):
recipeID(_recipeID),ingredientID(_ingredientID),quantity(_quantity),unitID(_unitID){}

void bindParam(UOrmStatement* stmt){
stmt->bindParam(U_ORM_TYPE_HANDLER(recipeID, uint32_t));
stmt->bindParam(U_ORM_TYPE_HANDLER(ingredientID, uint32_t));
stmt->bindParam(U_ORM_TYPE_HANDLER(quantity, double));
stmt->bindParam(U_ORM_TYPE_HANDLER(unitID, uint32_t));
}

void bindResult(UOrmStatement* stmt){
stmt->bindResult(U_ORM_TYPE_HANDLER(recipeID, uint32_t));
stmt->bindResult(U_ORM_TYPE_HANDLER(ingredientID, uint32_t));
stmt->bindResult(U_ORM_TYPE_HANDLER(quantity, double));
stmt->bindResult(U_ORM_TYPE_HANDLER(unitID, uint32_t));
}

void toJSON(UString& json){
U_TRACE(5, "RecipeIngredient::toJSON(%p)", &json)
json.toJSON(U_JSON_METHOD_HANDLER(recipeID, uint32_t));
json.toJSON(U_JSON_METHOD_HANDLER(ingredientID, uint32_t));
json.toJSON(U_JSON_METHOD_HANDLER(quantity, double));
json.toJSON(U_JSON_METHOD_HANDLER(unitID, uint32_t));
}

void fromJSON(UValue& json){
U_TRACE(5, "RecipeIngredient::fromJSON(%p)", &json)
json.fromJSON(U_JSON_METHOD_HANDLER(recipeID, uint32_t));
json.fromJSON(U_JSON_METHOD_HANDLER(ingredientID, uint32_t));
json.fromJSON(U_JSON_METHOD_HANDLER(quantity, double));
json.fromJSON(U_JSON_METHOD_HANDLER(unitID, uint32_t));
}

#ifdef DEBUG
const char* dump(bool breset) const
{ return U_NULLPTR; }
#endif 

private:
U_DISALLOW_ASSIGN(RecipeIngredient);
};
#endif
//////////////////////////////////////////////////////////////////////