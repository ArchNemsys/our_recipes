/*
* Copyright (C) 2013-2018 Jonathan Kelly jaknemsys@gmail.com
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
*    You should have received a copy of the License along with this program.
*    If not, see <https://www.gnu.org/licenses/>.
*
*    AUTO-GENERATED ORM WRAPPER
*
*    SELECT * FROM Units;
*/
//////////////////////////////////////////////////////////////////////
#ifndef UNIT_H
#define UNIT_H 1

#include <ulib/orm/orm.h>

class Unit
{
public:
  U_MEMORY_TEST
  U_MEMORY_ALLOCATOR
  U_MEMORY_DEALLOCATOR

	uint32_t unitID;
	UString notation;
	UString description;
	UString base_unit;
	UString scale_factor;

Unit(){}

Unit(uint32_t _unitID,UString _notation,UString _description,UString _base_unit,UString _scale_factor):
unitID(_unitID),notation(_notation),description(_description),base_unit(_base_unit),scale_factor(_scale_factor){}

void bindParam(UOrmStatement* stmt){
stmt->bindParam(U_ORM_TYPE_HANDLER(unitID, uint32_t));
stmt->bindParam(U_ORM_TYPE_HANDLER(notation, UString));
stmt->bindParam(U_ORM_TYPE_HANDLER(description, UString));
stmt->bindParam(U_ORM_TYPE_HANDLER(base_unit, UString));
stmt->bindParam(U_ORM_TYPE_HANDLER(scale_factor, UString));
}

void bindResult(UOrmStatement* stmt){
stmt->bindResult(U_ORM_TYPE_HANDLER(unitID, uint32_t));
stmt->bindResult(U_ORM_TYPE_HANDLER(notation, UString));
stmt->bindResult(U_ORM_TYPE_HANDLER(description, UString));
stmt->bindResult(U_ORM_TYPE_HANDLER(base_unit, UString));
stmt->bindResult(U_ORM_TYPE_HANDLER(scale_factor, UString));
}

void toJSON(UString& json){
U_TRACE(5, "Unit::toJSON(%p)", &json)
json.toJSON(U_JSON_METHOD_HANDLER(unitID, uint32_t));
json.toJSON(U_JSON_METHOD_HANDLER(notation, UString));
json.toJSON(U_JSON_METHOD_HANDLER(description, UString));
json.toJSON(U_JSON_METHOD_HANDLER(base_unit, UString));
json.toJSON(U_JSON_METHOD_HANDLER(scale_factor, UString));
}

void fromJSON(UValue& json){
U_TRACE(5, "Unit::fromJSON(%p)", &json)
json.fromJSON(U_JSON_METHOD_HANDLER(unitID, uint32_t));
json.fromJSON(U_JSON_METHOD_HANDLER(notation, UString));
json.fromJSON(U_JSON_METHOD_HANDLER(description, UString));
json.fromJSON(U_JSON_METHOD_HANDLER(base_unit, UString));
json.fromJSON(U_JSON_METHOD_HANDLER(scale_factor, UString));
}

#ifdef DEBUG
const char* dump(bool breset) const
{ return U_NULLPTR; }
#endif 

private:
U_DISALLOW_ASSIGN(Unit);
};
#endif
//////////////////////////////////////////////////////////////////////