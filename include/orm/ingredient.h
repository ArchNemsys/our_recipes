/*
* Copyright (C) 2013-2018 Jonathan Kelly jaknemsys@gmail.com
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
*    You should have received a copy of the License along with this program.
*    If not, see <https://www.gnu.org/licenses/>.
*
*    AUTO-GENERATED ORM WRAPPER
*
*    SELECT * FROM Ingredients;
*/
//////////////////////////////////////////////////////////////////////
#ifndef INGREDIENT_H
#define INGREDIENT_H 1

#include <ulib/orm/orm.h>

class Ingredient
{
public:
  U_MEMORY_TEST
  U_MEMORY_ALLOCATOR
  U_MEMORY_DEALLOCATOR

	uint32_t ingredientID;
	UString description;

Ingredient(){}

Ingredient(uint32_t _ingredientID,UString _description):
ingredientID(_ingredientID),description(_description){}

void bindParam(UOrmStatement* stmt){
stmt->bindParam(U_ORM_TYPE_HANDLER(ingredientID, uint32_t));
stmt->bindParam(U_ORM_TYPE_HANDLER(description, UString));
}

void bindResult(UOrmStatement* stmt){
stmt->bindResult(U_ORM_TYPE_HANDLER(ingredientID, uint32_t));
stmt->bindResult(U_ORM_TYPE_HANDLER(description, UString));
}

void toJSON(UString& json){
U_TRACE(5, "Ingredient::toJSON(%p)", &json)
json.toJSON(U_JSON_METHOD_HANDLER(ingredientID, uint32_t));
json.toJSON(U_JSON_METHOD_HANDLER(description, UString));
}

void fromJSON(UValue& json){
U_TRACE(5, "Ingredient::fromJSON(%p)", &json)
json.fromJSON(U_JSON_METHOD_HANDLER(ingredientID, uint32_t));
json.fromJSON(U_JSON_METHOD_HANDLER(description, UString));
}

#ifdef DEBUG
const char* dump(bool breset) const
{ return U_NULLPTR; }
#endif 

private:
U_DISALLOW_ASSIGN(Ingredient);
};
#endif
//////////////////////////////////////////////////////////////////////