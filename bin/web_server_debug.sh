#!/bin/sh

export ORM_DRIVER="sqlite"
#export UMEMPOOL="581,0,0,59,16409,-7,-20,-23,31"
export ORM_OPTION="host=localhost user=dbuser password=dbpass character-set=utf8 dbname=../db/cookbook"

# Debug Settings
mkdir -p ../log
export UTRACE="0 20M 0"
export UTRACE_SIGNAL="0 20M 0"
export UTRACE_FOLDER="../log"
# UOBJDUMP="0 10M 100"
# USIMERR="error.sim"

#printenv

# clean old logs
rm -f ../log/*

gdb -i=mi -ex=r --args userver_tcp -c ../etc/userver.cfg
#userver_tcp -c ../etc/userver.cfg
