#!/bin/bash
##############################################################################
 #
 #  application name: dnsactual
 #  other files: dnsactual.conf (keeps the last updated ip)
 #               dnsactual.log  (register date & time of the actualization)
 #  Author: Ernest Danton
 #  Date: 01/29/2007
 ##############################################################################

 if test -f ../etc/dnsactual.conf
   then
   CacheIP=$(cat dnsactual.conf)
 fi
#echo -e $CacheIP '\n'

askIP=$(wget http://freedns.afraid.org/dynamic/check.php -o /dev/null -O /dev/stdout)
CurreIP="${askIP#*: }"
CurreIP="${CurreIP%%$'\n'*}"

#echo -e  $CurreIP '\n'

 if [ "$CurreIP" = "$CacheIP" ]
 then
   # Both IP are equal
   echo "Update not required..."
 else
   # The IP has change
   echo "Updating http://free.afraid.org with " $CurreIP
   #wget https://freedns.afraid.org/dynamic/update.php?UWdkZXBSdTNuOU41WFBrb3g1UHJMZExNOjE5MTI5NzUy -o /dev/null -O /dev/stdout
   # echo `date`  "Updating log with IP " $CurreIP >> dnsactual.log
 fi
 rm -f dnsactual.conf
 echo $CurreIP > dnsactual.conf
