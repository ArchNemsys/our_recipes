BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "Recipes" (
	"recipeID"	INTEGER,
	"title"	TEXT NOT NULL,
	"prep_time"	INTEGER NOT NULL,
	"cook_time"	INTEGER NOT NULL,
	"servings"	INTEGER NOT NULL,
	"catergory"	TEXT NOT NULL,
	"cuisine"	TEXT NOT NULL,
	"meat"	TEXT NOT NULL,
	"method"	TEXT NOT NULL,
	"carbs"	REAL NOT NULL,
	"fiber"	REAL NOT NULL,
	"protein"	REAL NOT NULL,
	"fat"	REAL NOT NULL,
	"kcal"	REAL NOT NULL,
	PRIMARY KEY("recipeID")
);
CREATE TABLE IF NOT EXISTS "RecipeIngredients" (
	"recipeID"	INTEGER,
	"ingredientID"	INTEGER,
	"quantity"	REAL NOT NULL,
	"unitID"	INTEGER NOT NULL,
	FOREIGN KEY("recipeID") REFERENCES "Recipes"("recipeID"),
	FOREIGN KEY("ingredientID") REFERENCES "Ingredients"("ingredientID"),
	PRIMARY KEY("recipeID","ingredientID")
);
CREATE TABLE IF NOT EXISTS "Ingredients" (
	"ingredientID"	INTEGER,
	"description"	INTEGER NOT NULL,
	PRIMARY KEY("ingredientID")
);
CREATE TABLE IF NOT EXISTS "Units" (
	"unitID"	INTEGER,
	"notation"	TEXT NOT NULL,
	"description"	TEXT NOT NULL,
	"base_unit"	INTEGER,
	"scale_factor"	REAL NOT NULL,
	FOREIGN KEY("base_unit") REFERENCES "Units"("unitID"),
	PRIMARY KEY("unitID")
);
COMMIT;
