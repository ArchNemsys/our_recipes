async function getRecipes() {
    try {
        const response = await fetch("recipesCRUD");

        var options = {
            valueNames: [ 'recipeID', 'title', 'prep_time', 'cook_time', 'servings', 'method', 'carbs', 'fiber', 'fat', 'protein', 'kcal'
                        ],
            item: '<tr><td class="recipeID"></td><td class="title"></td><td class="prep_time"></td><td class="cook_time"></td><td class="servings"></td><td class="method"></td><td class="carbs"></td><td class="fibre"></td><td class="fat"></td><td class="protein"></td><td class="kcal"></td></tr>'
        };

        var recipesList = new List('recipes', options, await response.json());
    }
    catch (err) {
        console.log('fetch failed', err);
    }
}



/*
  userList.add({
  name: "Gustaf Lindqvist",
  born: 1983
  });
*/
